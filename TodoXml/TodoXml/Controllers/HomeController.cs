﻿using TodoXml.Models;
using System.Web.Mvc;
using System.Xml;

namespace TodoXml.Controllers 
{

    public class HomeController : Controller
    {
        public ActionResult Index()
        {

            return View();
        }
        public ActionResult Tasks()
        {
            XmlDocument XmlDocObj = new XmlDocument();
            XmlDocObj.Load(Server.MapPath("../Todo.xml"));
            XmlNode RootNode = XmlDocObj.SelectSingleNode("tasks");
            XmlNodeList task = RootNode.ChildNodes;


            return View(task);

        }
        public ActionResult Add(Tasks t) 
        {
            XmlDocument XmlDoc = new XmlDocument();
            XmlDoc.Load(Server.MapPath("../Todo.xml"));
            XmlNode RootNode = XmlDoc.SelectSingleNode("tasks");

            int taskNumber = RootNode.ChildNodes.Count;
            XmlNode taskNode = RootNode.AppendChild(XmlDoc.CreateNode(XmlNodeType.Element, "note", ""));
            taskNode.AppendChild(XmlDoc.CreateNode(XmlNodeType.Element, "Id", "")).InnerText = taskNumber.ToString();
            taskNode.AppendChild(XmlDoc.CreateNode(XmlNodeType.Element, "task", "")).InnerText = t.task.ToString();
            taskNode.AppendChild(XmlDoc.CreateNode(XmlNodeType.Element, "due", "")).InnerText = t.due;
            


            XmlDoc.Save(Server.MapPath("../Todo.xml"));


            return RedirectToAction("Index");

        }
        public ActionResult Search(string id) 
        {
            XmlDocument XmlDoc = new XmlDocument();
            XmlDoc.Load(Server.MapPath("../../Todo.xml"));
            XmlNode RootNode = XmlDoc.SelectSingleNode("tasks");
            XmlNodeList tasks = RootNode.ChildNodes;

            XmlNode t = tasks[0];

            foreach (XmlNode ta in tasks)
            {
                if (ta["Id"].InnerText == id)
                {
                    t = ta;
                }
            }

            return View(t);

        }
        public ActionResult Update(string id)
        {
            XmlDocument XmlDoc = new XmlDocument();
            XmlDoc.Load(Server.MapPath("../../Todo.xml"));
            XmlNode RootNode = XmlDoc.SelectSingleNode("tasks");
            XmlNodeList tasks = RootNode.ChildNodes; 

            XmlNode t = tasks[0];

            foreach (XmlNode ta in tasks)
            {
                if (ta["Id"].InnerText == id)
                {
                    t = ta;
                }
            }

            return View(t);

        }
        public ActionResult Update2(Tasks t) 
        {
            XmlDocument XmlDocObj = new XmlDocument();
            XmlDocObj.Load(Server.MapPath("../Todo.xml"));
            XmlNode RootNode = XmlDocObj.SelectSingleNode("tasks");
            XmlNodeList tasks = RootNode.ChildNodes;

            XmlNode task = tasks[0];

            foreach (XmlNode ta in tasks)
            {
                if (ta["Id"].InnerText == t.id.ToString())
                {
                    task = ta;
                }
            }

            task["task"].InnerText = t.task;
            task["due"].InnerText = t.due;
            

            XmlDocObj.Save(Server.MapPath("../Todo.xml"));

            return RedirectToAction("tasks");

        }
        public ActionResult Delete(string id)
        {
            XmlDocument XmlDocObj = new XmlDocument();
            XmlDocObj.Load(Server.MapPath("../../Todo.xml"));
            XmlNode RootNode = XmlDocObj.SelectSingleNode("tasks");
            XmlNodeList tasks = RootNode.ChildNodes;

            XmlNode t = tasks[0];

            foreach (XmlNode task in tasks)
            {
                if (t["Id"].InnerText == id)
                {
                    t = task;
                }
            }

            RootNode.RemoveChild(t);

            XmlDocObj.Save(Server.MapPath("../../Todo.xml"));

            return RedirectToAction("Tasks");
        }
    }
}